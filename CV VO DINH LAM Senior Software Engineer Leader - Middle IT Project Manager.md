## Senior Software Engineer Leader - Middle IT Project Manager

> VO DINH LAM
> vodinhlam2010@gmail.com
> 0837-942-038

## Summary

Energetic IT Project Manager with 2 years of experience in collaborating with business to point out pain points, collect requirement, planning, overseeing the preparation and dissemination of project communications then controlling, executing, training, identifying resources needed, managing and driving vendors to complete various IT project in a timely manner.
Able to lead a team of developers follow agile and scrum methodology. Preparing requests for proposals and conducting all necessary meetings to facilitate selection of project services and products. Experience working across business units, understanding their issues and managing their expectations. Experience in ERP as the back-office system for ecommerce: OMS, marketing, promotion, loyalty, inventory, fulfillment, logistics & warehouse integration…

Highly-skilled and motivated senior software engineer with 8+ years of experience. Enhanced performance of many applications using PHP, Java Spring Boot, Nodejs, C#, Golang, Vuejs, Reactjs, Redux, MySQL, Postgres and HA Design. Increased revenue by analyzing and improving app monetization strategies. Seeking to draw on proven software development and engineering skills to increase and improve impressive line of applications.

## Experience

#### Software Engineer Team Leader
##### ASIM Group
2022-present
Over saw software development and coded profitable apps using C#, Java Sping Boot, Nodejs, Reactjs, MariaDB, Stellar.
Deployed/Patched/upgraded 80+ servers, applications, databases and HA Design.

> Key achievement
- Contributed to a 50% increase in sales for 1 years in a row through increasing software performance and hardware performance.

> Projects
- Merchant Backend
- Vender Service
- Simstore API
- Localshop Backend
- Merchant Webapp
- Localshop Sim
- Localshop Esim FE
- Localshop Esim BE
- Localshop Admin BE
- Localshop Admin FE
- Localpay Asim Gateway
- EKYC
- Stellar
- And more

> Technical Design
- Java
- C#
- Nodejs
- Golang
- Reactjs
- MariaDB
- Redis
- AWS - S3
- RabbitMQ
- Docker
- K8s
- Rancher
- HAProxy
- KeepAlived
- Ubuntu
- Git

#### IT Project Manager
##### Playground Inc.
2020-2022
Managed a team of 30+ multilevel of IT developers.
90% of IT projects were completed ahead of schedule and within budget.
Deployed/Patched/upgraded 64 servers, applications and databases.

> Key Achievement
- Managed deployment of https://winery.finance/ in 4 weeks.

> Projects
- https://snailhouse.io/
- https://bitstoragebox.com/
- https://creaturehunters.world/
- https://nftmarble.games/
- https://bingo.family/
- https://12guardian.com/
- And more

> Technical Design
- PHP
- Nodejs
- Nextjs
- Solidity
- Reactjs
- Vuejs
- Docker
- AWS - S3
- CDN
- MySQL (AWS - EC2)
- Redis (AWS - EC2)
- Ubuntu (AWS - EC2)
- Git

#### Senior Fullstack PHP Leader
##### Inceptionit Inc.
2018-2019
Over saw software development and coded profitable apps using PHP, HTML, Javascript.
Deployed/Patched/upgraded 10 servers, applications, databases in a large of 203 server of Golftech Group.

> Projects
- Skills Challenge
- Game Plan Builder
- Age Verify

> Key achievement
- Game Plan Builder

> Technical Design
- PHP
- HTML
- Javascript
- MySQL (AWS - RDS)
- Ubuntu (AWS - EC2)
- Git

#### Senior PHP Backend Web developer
##### Silkwires Inc.
2018-2018
Using Symfony 2 framework, MySQL for coding POS project

> Projects
- POS

> Key achievement
- POS

> Technical Design
- PHP
- Symfony
- HTML
- Javascript
- MySQL (AWS - RDS)
- Ubuntu (AWS - EC2)
- Git

#### Frontend Web Leader
##### Prozy Inc.
2016-2018
Using PHP Laravel framework, javascrip, reactjs, jQuery and build projects

> Products
- POS
- PAM

> Key achievement
- PAM

> Technical Design
- PHP
- Laravel
- Reactjs
- CDN
- HTML
- Javascript/jQuery
- Git

#### PHP Fullstack web developer
##### Targrem Inc.
2012-2015
Using PHP Yii 1 framework, javascript, jQuery, HTML, CSS coded CMS, CRM for serta.com and some small projects

> Projects
- Serta.com Admin
- Serta.com CRM
- Indition CMS
- And more

> Key achievement
- Indition CMS - Product management

> Technical Design
- PHP
- Yii
- HTML
- CDN
- Javascript/jQuery
- Postgres
- MySQL
- CSS
- Git

#### PHP Teacher + wordpress developer
##### Thai Binh Duong EDU
2010-2012
PHP Teacher and freelancer wordpress developer

> Technical Design
- PHP
- HTML
- Javascript/jQuery
- MySQL
- CSS
- Git

## Education
2005-2010, HO CHI MINH CITY UNIVERSITY OF SCIENCE
 
## Soft Skills
UX/UI & digital products
Project management
Time management
Leadership
Excellent knowledge of all major operating systems
Strategic planning
Critical Thinking
Problem Solving
Mathematics
Effective Communication
Teamwork
 
## Additional Information
English - listening - mid
English - reading/writing - good

## Hobbies
Team building
